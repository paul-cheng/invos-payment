import unittest

from src.domain.county_parking_fee import CountyParkingFeePaymentService
from src.repository import InMemoryRepository
from src.vendor.parking import FakePiCountyParkingClient
from src.vendor.payment import FakePchomepayClient


class ServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.parking_client = FakePiCountyParkingClient()
        self.payment_client = FakePchomepayClient()
        self.repo = InMemoryRepository()
        self.service = CountyParkingFeePaymentService(
            self.parking_client, self.payment_client, self.repo
        )
        self.access_token = "tester"

    def test_query_bills_success(self):
        bills_req = dict(county_code="TPE", vehicle_type="汽車", vehicle_no="ABC-1234")
        assert bills_req == self.service._bills_req_schema.load(bills_req)
        bills, _status_code = self.service.query_bills(bills_req)
        assert bills == self.service._bills_resp_schema.load(bills)

    def test_query_bills_failed(self):
        pass

    def test_create_payment_order_success(self):
        order_req = dict(total_amount=100, bill_count=2, bills=["b0000001", "b0000002"])
        assert order_req == self.service._order_req_schema.load(order_req)
        order_resp, _status_code = self.service.create_payment_order(
            self.access_token, order_req
        )
        assert order_resp == self.service._order_resp_schema.load(order_resp)
        order = self.repo.get_payment_order(order_resp["order_id"])
        assert order["order_id"] == order_resp["order_id"]
        assert order["total_amount"] == order_resp["total_amount"]

        order_resp2, _status_code = self.service.get_payment_order(
            self.access_token, order["order_id"]
        )
        assert order["order_id"] == order_resp2["order_id"]
        assert order["total_amount"] == order_resp2["total_amount"]

    def test_process_payment_notification_success(self):
        pass
