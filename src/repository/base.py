import abc


class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def get_app_id(self, access_token) -> "AppId":
        pass

    @abc.abstractmethod
    def add_payment_order(self, order) -> None:
        pass

    @abc.abstractmethod
    def get_payment_order(self, order_id) -> "PaymentOrder":
        pass


class InMemoryRepository(AbstractRepository):
    def __init__(self):
        self.data = {"apps": {"tester": "tester"}, "orders": {}}

    def get_app_id(self, access_token):
        return self.data["apps"][access_token]

    def add_payment_order(self, order_req):
        self.data["orders"][order_req["order_id"]] = order_req

    def get_payment_order(self, order_id):
        return self.data["orders"].get(order_id)
