import abc


class AbstractPaymentClient(abc.ABC):
    @abc.abstractmethod
    def create_payment_order(self, order_req) -> "PaymentOrder":
        pass


class FakePchomepayClient(AbstractPaymentClient):
    def __init__(self):
        pass

    def create_payment_order(self, order_req):
        return {
            "order_id": order_req["order_id"],
            "payment_url": "https://fake.payment.url/" + order_req["order_id"],
        }
