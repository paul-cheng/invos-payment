import abc
from copy import deepcopy
from src.domain.county_parking_fee import BillStatus


class AbstractParkingClient(abc.ABC):
    vehicle_type_map = {"汽車": "C", "機車": "M"}

    @abc.abstractmethod
    def query_bills(self, bills_req) -> "BillsResponse":
        pass


class FakePiCountyParkingClient(AbstractParkingClient):
    def __init__(self):
        self.data = {
            "TPE": {
                "C:ABC-1234": {
                    "status": "success",
                    "bill_count": 2,
                    "total_amount": 50,
                    "bill_list": [
                        {
                            "bill_no": "b000001",
                            "bill_type": "一般停車單",
                            "parking_date": "2018-08-30",
                            "expire_date": "2018-09-20",
                            "amount": 20,
                        },
                        {
                            "bill_no": "b000002",
                            "bill_type": "一般停車單",
                            "parking_date": "2018-08-30",
                            "expire_date": "2018-09-20",
                            "amount": 30,
                        },
                    ],
                }
            }
        }

    def query_bills(self, bills_req):
        vtype = self.vehicle_type_map.get(bills_req["vehicle_type"])
        vno = bills_req["vehicle_no"]
        result = self.data.get(bills_req["county_code"], {}).get(f"{vtype}:{vno}", {})
        result["bills"] = []
        for bill in result["bill_list"]:
            bill["bill_status"] = BillStatus.UNPAID.value
            result["bills"].append(bill)

        result = deepcopy(result)
        del result["bill_list"], result["status"]
        return result
