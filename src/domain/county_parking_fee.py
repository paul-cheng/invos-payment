import enum
import pytz
from datetime import datetime
from uuid import uuid4

import regex
from marshmallow import Schema, ValidationError, fields, validate

TIMEZONE = pytz.timezone("Asia/Taipei")
COUNTIES_MAP = {
    "TPE": "台北市",
    "MLH": "苗栗縣",
    "CHG": "彰化縣",
    # "CIH": "嘉義市",  # 約定查詢
    "TNH": "台南市",
    "KHC": "高雄市",
    # "ILH": "宜蘭縣",  # 約定查詢
    "HLH": "花蓮縣",
}


class BillStatus(enum.Enum):
    UNPAID = "尚未繳款"
    PAID = "已繳款"
    UNREFUND = "待退款"
    REFUNDED = "已退款"
    CLOSE = "已銷案"


class OrderStatus(enum.Enum):
    CREATED = "已建立"
    EXPIRED = "已過期"
    PAID = "已支付"


county_code_field = fields.String(
    required=True, validate=validate.OneOf(list(COUNTIES_MAP.keys()))
)
bill_no_field = fields.String(required=True)
bill_status_field = fields.String(
    required=True, valdiate=validate.OneOf([e.value for e in BillStatus])
)
order_id_field = fields.String(
    required=True, validate=validate.Regexp(r"\d{8}_[0-9a-z]{32}")
)
order_status_field = fields.String(
    required=True, validate=validate.OneOf([e.value for e in OrderStatus])
)
int_gt0_field = fields.Int(required=True, validate=validate.Range(min=1))


class CountySchema(Schema):
    county_code = county_code_field
    county_name = fields.String(validate=validate.OneOf(list(COUNTIES_MAP.values())))


class CountiesSchema(Schema):
    counties = fields.List(fields.Nested(CountySchema))


class BillsReqSchema(Schema):
    county_code = county_code_field
    vehicle_type = fields.String(required=True, validate=validate.OneOf(["汽車", "機車"]))
    vehicle_no = fields.String(
        required=True,
        validate=validate.Regexp(
            r"^[a-z0-9]{3,4}-[a-z0-9]{3,4}$",
            regex.IGNORECASE,
            error="車牌號碼需合規，例：198-GEW、GERR-1933",
        ),
    )


class BillSchema(Schema):
    bill_type = fields.String(required=True)
    bill_no = bill_no_field
    bill_status = bill_status_field
    amount = int_gt0_field
    parking_date = fields.String(required=True)
    expire_date = fields.String(required=True)


class BillsRespSchema(Schema):
    total_amount = int_gt0_field
    bill_count = int_gt0_field
    bills = fields.List(fields.Nested(BillSchema))


class OrderReqSchema(Schema):
    total_amount = int_gt0_field
    bill_count = int_gt0_field
    bills = fields.List(bill_no_field)


class OrderSchema(Schema):
    total_amount = int_gt0_field
    bill_count = int_gt0_field
    bills = fields.List(bill_no_field)
    app_id = fields.String(required=True)
    order_id = order_id_field
    order_status = order_status_field
    payment_url = fields.String()


class OrderRespSchema(Schema):
    total_amount = int_gt0_field
    bill_count = int_gt0_field
    bills = fields.List(bill_no_field)
    order_id = order_id_field
    order_status = order_status_field
    payment_url = fields.String(required=True)


class CountyParkingFeePaymentService:
    name = "county-parking-fee"

    _bills_req_schema = BillsReqSchema()
    _bills_resp_schema = BillsRespSchema()
    _order_req_schema = OrderReqSchema()
    _order_schema = OrderSchema()
    _order_resp_schema = OrderRespSchema()

    def __init__(self, parking_client, payemnt_client, repo) -> None:
        self.parking_client = parking_client
        self.payment_client = payemnt_client
        self.repo = repo

    def query_bills(self, bills_req) -> "BillsResponse":
        try:
            bills_req = self._bills_req_schema.load(bills_req)
        except ValidationError as e:
            return {"error_messages": e.messages}, 403
        result = self.parking_client.query_bills(bills_req)
        return result, 200

    def create_payment_order(self, access_token, order_req) -> "OrderResponse":
        try:
            order_req = self._order_req_schema.load(order_req)
        except ValidationError as e:
            return {"error_messages": e.messages}, 403
        app_id = self.repo.get_app_id(access_token)
        now = datetime.now()
        order_req = {
            **order_req,
            **{
                "app_id": app_id,
                "order_id": now.strftime("%Y%m%d_") + uuid4().hex,
                "order_status": OrderStatus.CREATED.value,
            },
        }
        order = self._order_schema.load(order_req)
        self.repo.add_payment_order(order)
        order = self.repo.get_payment_order(order["order_id"])
        order_resp = self.payment_client.create_payment_order(order)
        return self._order_resp_schema.dump({**order, **order_resp}), 200

    def get_payment_order(self, access_token, order_id) -> "OrderResponse":
        order = self.repo.get_payment_order(order_id)
        if order:
            return self._order_resp_schema.dump(order), 200
        return {"error_messages": ["Order not found"]}, 404

    def process_payment_notification(self, req):
        """
        1. update every bill status
        2. submit paid bills through parking client
        3. notify app
        """
        pass

    def process_refunded_bills(self, order_date):
        pass

    def notify_refunded_bills(self):
        pass
