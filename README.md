## Commands

```
# install dependencies
pip install -r requirements.txt

# install pre-commit hooks
pre-commit install

# run an instance into http://localhost:5000/api/docs
python -m src.application.rest

# test
python -m pytest -vvvvv
```
